# ansible-role-template

An Ansible role template.

Start using it with:

```bash
$ molecule init template --url git@gitlab.com:autonomic-roles/ansible-role-template.git
```
